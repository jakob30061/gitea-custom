<style>
  /* bthree dark theme */
  @font-face {
    font-family: "Heebo";
    src: url("/assets/fonts/Heebo-VariableFont_wght.ttf") format("truetype-variations");
    font-weight: normal;
    font-variation-settings: "wght" var(--font-weight);
    font-style: normal
  }

  :root {
    /* Spacing. */
    --spacer: 1rem;
    --spacer-1: .25rem;
    --spacer-2: .5rem;
    --spacer-3: var(--spacer);
    --spacer-4: 1.5rem;
    --spacer-5: 3rem;

    /* Override fonts. */
    --fonts-override: 'Heebo', -apple-system, BlinkMacSystemFont, "Segoe UI", Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    --font-weight-thin: 300;
    --font-weight-bold: 600;

    --border-radius: .33rem;

    --color-border: var(--color-secondary);

    /* Transitions. */
    --transition-speed: 150ms;
  }

  /* Light-theme specific. */
  .theme-gitea {
    --color-primary: hsl(204deg, 90%, 52%);
  }

  /* Set weight variations. */
  strong, b, h1, h2, h3, h4, h5, h6, .bold,
  a.issue-title,
  .ui.tabular.menu .active.item {
    font-weight: normal !important;
    font-variation-settings: "wght" var(--font-weight-bold);
  }

  .repository.view.issue .title .issue-title h1 {
    font-variation-settings: "wght" var(--font-weight-thin);
  }

  @media (min-width: 1200px) {
    /* Limit navbar width on large screens. */
    .dashboard-navbar {
      margin-left: auto;
      margin-right: auto;
      max-width: 1170px;
    }
  }

  /* Revert the red color in dashboard eader buttons. */
  .dashboard.feeds .right.stackable.menu>.item.active,
  .dashboard.issues .right.stackable.menu>.item.active {
    color: unset;
  }

  /* Global tweaks (all themes) */
  .ui.avatar {
    border-radius: 50%;
  }

  .ui.user.list .ui.avatar,
  .ui.user.list .ui.avatar+.content {
    display: inline-block;
  }

  /* Fix alignment and margin issues in avatars. */
  .user.list img.avatar,
  .comment-list .code-comment img.avatar,
  .comment-list .comment img.avatar {
    vertical-align: unset !important;
  }

  .user.list img.avatar,
  .commit-list img.avatar,
  .comment-list .code-comment img.avatar,
  .comment-list .comment img.avatar,
  .comment-list .timeline-item img.avatar {
    margin-right: var(--spacer-2) !important;
  }

  .commit-list .author img.avatar {
    margin-right: var(--spacer-3) !important;
  }

  /* Labels. e.g. issues count indicator in repo tabs. */
  .ui.small.label {
    border-radius: 2em;
  }

  /* Hide the app logo (first link in "brand" div). */
  .following.bar #navbar .brand > a:first-child {
    display: none;
  }

  .following.bar #navbar .brand #navbar-expand-toggle {
    margin-left: var(--spacer-4);
  }

  /* Hide brand navbar completely when not in mobile. */
  @media (min-width: 768px) {
    .following.bar #navbar .brand {
      display: none;
    }
  }

  /* Notifications indicator. */
  .notification_count {
    border-radius: 0.6em;
    font-size: 12px;
    font-weight: normal !important;
    font-variation-settings: "wght" var(--font-weight-bold);
    line-height: unset;
    margin-left: var(--spacer-1);
    padding: 2px 4px;
  }

  /* Navigation tabs.
  * e.g. Code, Issues pages in a repository. */
  .ui.tabular.menu .item {
    border: none;
    border-bottom: 3px solid transparent;
    border-radius: 0;
    transition: border-bottom-color var(--transition-speed) ease-in-out, color var(--transition-speed) ease-in-out;
  }

  .ui.tabular.menu .item:hover {
    border-bottom-color: var(--color-text-dark);
  }

  .ui.tabular.menu .active.item {
    background: transparent;
    border: none;
    border-bottom: 3px solid currentColor;
    margin-bottom: unset;
  }

  .ui.tabular.menu .active.item:hover {
    background: transparent;
    border-color: var(--color-text-dark);
    color: var(--color-text-dark);
  }

  /* Repository page. */
  .repository .repo-title .repo-icon svg {
    max-width: 22px;
  }

  .ui.table {
    background: transparent;
  }

  .repository .ui.table a {
    color: var(--color-text);
  }

  .repository .file-view.markdown a {
    color: var(--color-primary);
  }

  .repository .file-view .markup img {
    border-radius: var(--border-radius);
  }

  /* Repository folder icon. */
  .repository.file.list #repo-files-table tbody .svg.octicon-file-directory-fill {
    color: currentColor;
  }

  .ui.repo-topic.label {
    border-radius: 2em;
  }

  /* Repository stats. */
  .repository-summary-language-stats {
    height: unset;
  }

  .repository .repository-summary .segment.language-stats {
    height: 4px;
  }

  /* Repository: Activity page. */
  .stats-table {
    border-radius: 2em;
    margin-bottom: var(--spacer);
    overflow: hidden;
  }

  .issue.list {
    border-radius: var(--border-radius);
    border: thin solid var(--color-border);
  }

  .issue.list > .item {
    padding: var(--spacer-2) var(--spacer) !important;
  }

  /* Hide branches from PR list. */
  .issue.list .branches {
    display: none !important;
  }

  .issue.list a.project,
  .issue.list a.milestone {
    margin-inline: var(--spacer-2) !important;
  }

  .issue.list a.project svg {
    margin-right: var(--spacer-2) !important;
    position: relative;
    top: 2px;
  }

  /* Footer. */
  footer {
    color: var(--color-footer-text);
  }

  footer a {
    color: var(--color-footer-links);
  }

  /* Manual preview tweaks. */
  .restructuredtext a {
    color: var(--color-accent) !important;
  }
  .restructuredtext :is(section, dd) > div {
    background-color: var(--color-markup-code-block);
    padding-bottom: 8px;
    margin-bottom: 16px;
    padding: 0;
    border-radius: 5px;
  }
  .restructuredtext section div > p:first-child {
    font-weight: 700;
  }
  .restructuredtext section div * {
    margin: 0 !important;
  }
  .restructuredtext section div > :is(p, dl) {
    padding: 8px 12px;
  }
  .restructuredtext section div > ul {
    padding-bottom: 12px;
  }
  .restructuredtext section dl dt {
    font-style: normal;
  }
</style>

<script defer data-domain="projects.blender.org" src="https://analytics.blender.org/js/script.js"></script>
